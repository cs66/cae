$(function(){
  $.ajax({
    url:'doc.json',
    dataType:'json',
    success:function(doc){
      $('body').data('doc',doc); // This saves the doc object
      $('#bcf').text(doc.bcf);   // bcf is "balance carried forward"
      $('#odl').text(doc.odl);
      $('#cname').text(doc.cname);
      $('#caddr').text(doc.caddr.street + ', '+doc.caddr.town + ', ' + doc.caddr.pc);
      $('<tr/>')
        .append($('<td/>',{text:doc.spf}))
        .append($('<td/>',{text:'Balance carried forward'}))
        .append($('<td/>',{text:doc.bcf,'class':'num'}))
        .append($('<td/>',{'class':'num'}))
        .append($('<td/>',{text:doc.bcf,'class':'num'}))
        .appendTo('#transactions');
      let rt = doc.bcf;
      for(let t of doc.trn){
        rt += t.amt;
        let cr = $('<tr/>')
          .data('amt',t.amt)
          .append($('<td/>',{text:t.whn}))
          .append($('<td/>',{text:t.nar}))
          .appendTo('#transactions');
        if (t.amt>0){
          cr.append($('<td/>',{text:t.amt,'class':'num'}))
            .append($('<td/>'));
        }else{
          cr.append($('<td/>'))
            .append($('<td/>', {text:-t.amt,'class':'num'}));
        }
        cr.append($('<td/>',{text:rt,'class':'num'})) 
      }
      $('#transactions td').click(function(){
        $(this).parent().toggleClass('picked');
        let tot = 0;
        $('.picked').each(function(i,e){
          tot += $(e).data('amt');
        });
        $('#popup').remove();
        $('<div/>',{text:$('.picked').length + ' items selected total '+ tot, 'id':'popup'})
          .prependTo(this);
      });

      //Categories
      let catHash = {};
      for(let t of doc.trn){
        let key = t.nar.split(' - ',1)[0];
        catHash[key] = (catHash[key] || 0) + t.amt;
      }
      $('#cdetails').append(mkTable(catHash,
         {caption:'Spend Categories'}));
      console.log(catHash);
    }});
});

function mkTable(h,opts){
  let ret = $('<table/>');
  if (opts && opts.caption !== undefined)
    ret.append($('<caption/>',{text:opts.caption}));
  for(let k in h){
    ret.append(
      $('<tr/>')
        .append($('<td/>',{text:k}))
        .append($('<td/>',{text:h[k]}))
      )
  }
  return ret;
}















